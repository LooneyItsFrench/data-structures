# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
# 
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.

class LinkedQueue():
    def __init__(self):
        self.head = None
        self.tail = None

    def dequeue(self):
        raise Exception()

    def enqueue(self, head=None, tail=None):
        self.head = head
        self.tail = tail
        
class LinkedQueueNode:
    def __init__(self, value, link=None):
        self.value = value
        self.link = link
        

